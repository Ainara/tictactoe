# Función para dibujar el tablero
def dibujar_tablero(tablero):
    print(" ")
    print("\t "+tablero[7]+" | "+tablero[8]+" | "+tablero[9]+" ")
    print("\t---+---+---")
    print("\t "+tablero[4]+" | "+tablero[5]+" | "+tablero[6]+" ")
    print("\t---+---+---")
    print("\t "+tablero[1]+" | "+tablero[2]+" | "+tablero[3]+" ")
    print(" ")

# Función para verificar si alguien ha ganado
def verificar_ganador(tablero, jugador):
    combinaciones_ganadoras = [
        [1, 2, 3], [4, 5, 6], [7, 8, 9],  # Filas
        [1, 4, 7], [2, 5, 8], [3, 6, 9],  # Columnas
        [1, 5, 9], [3, 5, 7]  # Diagonales
    ]
    for combinacion in combinaciones_ganadoras:
        if tablero[combinacion[0]] == tablero[combinacion[1]] == tablero[combinacion[2]] == jugador:
            return True
    return False

# Función para jugar
def jugar():
    tablero = [" "]*10
    jugador_actual = "X"
    juego_terminado = False

    while not juego_terminado:
        dibujar_tablero(tablero)
        movimiento = int(input("Jugador " + jugador_actual + ", ingresa tu movimiento (1-9): "))

        if tablero[movimiento] == " ":
            tablero[movimiento] = jugador_actual

            if verificar_ganador(tablero, jugador_actual):
                dibujar_tablero(tablero)
                print("¡Jugador " + jugador_actual + " ha ganado!")
                juego_terminado = True
            elif " " not in tablero[1:]:
                dibujar_tablero(tablero)
                print("¡Es un empate!")
                juego_terminado = True
            else:
                jugador_actual = "O" if jugador_actual == "X" else "X"
        else:
            print("¡Movimiento inválido! Inténtalo de nuevo.")

# Iniciar el juego
jugar()